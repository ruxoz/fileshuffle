﻿namespace FileShuffler

open System.IO
open Perst

type LocationType =
    | Origin = 0
    | ShuffleFolder = 1

type FileEntry() =
    inherit Persistent()

    /// <summary>
    /// Pure path without drive.
    /// </summary>
    [<DefaultValue>] val mutable OriginPath : string
    /// <summary>
    /// Pure path without drive in the shuffle directory.
    /// </summary>
    [<DefaultValue>] val mutable ShuffledPath : string
    [<DefaultValue>] val mutable Location : LocationType

    static member private getPathOnly(path) =
        let pathRoot = Path.GetPathRoot(path)
        path.Substring(pathRoot.Length)

    static member FromFilePath(path) = FileEntry(OriginPath=FileEntry.getPathOnly(path), Location=LocationType.Origin)