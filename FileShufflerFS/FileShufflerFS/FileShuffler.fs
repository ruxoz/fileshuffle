﻿namespace FileShuffler

open System
open System.IO
open System.Collections.ObjectModel
open System.Windows

type ProgressBar() =
    inherit RZ.Wpf.ViewModelBase()

    let mutable count = 0
    let mutable value = 0

    member me.Count with get() = count and set(v) = me.setValue(&count, v, "Count")
    member me.Value with get() = value and set(v) = me.setValue(&value, v, "Value")

type MainWindowModel() as me =
    inherit RZ.Wpf.ViewModelBase()

    let drives = new ObservableCollection<DriveInfo>()
    let mutable selectedDriveIndex = 0
    let mutable statusText = System.String.Empty
    let logs = RZ.Wpf.WpfObservableCollection<string>()
    let mutable isBusy = false

    let logger = LogBroadcaster() :> ILog
    do logger.add_Log(fun s o -> me.Logs.Add(s + ": " + o.ToString()))
    let structureStorage = lazy(
                               let filePath = Path.Combine(me.SelectedDrive.RootDirectory.FullName, DefaultNames.FileName)
                               new FileStructureStorage(filePath)
                           )
    member me.Logs : RZ.Wpf.WpfObservableCollection<string> = logs
    member me.Drives = drives
    member me.IsBusy with get() = isBusy and set(v) = me.setValue(&isBusy, v, "IsBusy")
    member val Progress = new ProgressBar() with get
    member me.SelectedDriveIndex with get() = selectedDriveIndex and set(v) = me.setValue(&selectedDriveIndex, v, "SelectedDriveIndex")
    member me.SelectedDrive with get() : DriveInfo = drives.[selectedDriveIndex]
    member me.StatusText with get() = statusText and set(v) = me.setValue(&statusText, v, "StatusText")

    member private me.storage with get() = structureStorage.Force()
    member private me.increaseProgress() = me.Progress.Value <- me.Progress.Value + 1
    member private me.resetProgress(maxValue) =
        me.Progress.Count <- maxValue
        me.Progress.Value <- 0

    member private me.log(s) = me.Logs.Add("Info: " + s)
    member me.RefreshDrives() =
        DriveInfo.GetDrives() |> Array.iter (fun d -> me.Drives.Add(d))

    member private me.restoreFileStructure(selectedDrive, entries) =
        me.StatusText <- "Getting file list..."
        let shuffleDir = ShuffleStorage(selectedDrive, logger)

        me.resetProgress(List.length entries)
        me.StatusText <- "Restoring structure..."
        entries
        |> List.iter (fun fileEntry ->
                shuffleDir.RestoreFile(fileEntry)
                fileEntry.Location <- LocationType.Origin
                fileEntry.Modify()
                me.increaseProgress())
        ignore <| MessageBox.Show("File Structure is restored")
        me.IsBusy <- false

    member private me.restoreFiles(fileIterator: Collections.Generic.IEnumerable<FileEntry>) =
        let entries = Seq.toList fileIterator
        me.IsBusy <- true
        let selectedDrive = me.SelectedDrive.RootDirectory.FullName
        async { me.restoreFileStructure(selectedDrive, entries) }
        |> Async.Start

    member me.RestoreStructure(s: obj, e: RoutedEventArgs) =
        me.StatusText <- "Getting file list..."
        let entries = me.storage.GetShuffledFiles()

        me.Progress.Count <- entries.Length
        me.Progress.Value <- 0
        me.StatusText <- "Restoring..."
        me.restoreFiles(entries)

    member me.SaveStructure(sender: obj, args: System.Windows.RoutedEventArgs) =
        me.resetProgress(1)
        let rootDir = me.SelectedDrive.RootDirectory
        let fileEntries = rootDir.GetFileSystemInfos("*", SearchOption.AllDirectories)

        let excludeFile = (FileAttributes.System|||FileAttributes.Temporary|||FileAttributes.Directory)
        let stored = me.storage.Store(query {
                                            for entry in fileEntries do
                                            where ((entry.Attributes &&& excludeFile) = enum<FileAttributes>(0))
                                            select entry.FullName
                                        })
        me.StatusText <- sprintf "%d entries have been saved." stored
        ignore <| MessageBox.Show("File Structure is stored!")

    member this.CleanupBeforeClosing(s: obj, e: ComponentModel.CancelEventArgs) =
        e.Cancel <- false
        if structureStorage.IsValueCreated then
            (this.storage :> IDisposable).Dispose()

    member private me.moveFile (rootPath) (entries) =
        let shuffleDir = ShuffleStorage(rootPath, logger)
        entries
        |> Array.iter (fun (fileEntry: FileEntry) -> 
            let shuffledPath = shuffleDir.TakeFrom(fileEntry.OriginPath)
            fileEntry.ShuffledPath <- shuffledPath
            fileEntry.Location <- LocationType.ShuffleFolder
            fileEntry.Modify()
            me.increaseProgress())

    member val private random = Random()
    member private me.randomNot target range =
        let n = me.random.Next(range)
        if n = target then me.randomNot target range else n 

    member private me.shuffle(i, entries) =
        if i < Array.length(entries) then
            let target = me.randomNot (i) (Array.length entries)
            let temp = entries.[target]
            entries.[target] <- entries.[i]
            entries.[i] <- temp
            me.increaseProgress()

            me.shuffle (i+1, entries)
        
    member private me.shuffle(storage: FileStructureStorage, selectedDrive) =
        me.StatusText <- "Getting file list..."
        let entries = storage.GetOriginalSourceFiles() |> List.toArray

        me.resetProgress(entries.Length)
        me.StatusText <- "Shuffling..."
        me.shuffle (0, entries)

        me.resetProgress(entries.Length)
        me.StatusText <- "Moving file..."
        me.moveFile (selectedDrive) (entries)

        ignore <| MessageBox.Show("Files are randomized and rearranged!")

        me.IsBusy <- false

    member me.Shuffle(sender:obj, e: RoutedEventArgs) =
        me.IsBusy <- true
        let selectedDrive = me.SelectedDrive.RootDirectory.FullName
        Async.Start <| async { me.shuffle(me.storage, selectedDrive) }
