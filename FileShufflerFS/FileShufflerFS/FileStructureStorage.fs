﻿namespace FileShuffler

open System
open System.IO
open Perst

type private StorageRoot(db: Storage) =
    inherit Persistent()

    member val public Files = db.CreateFieldIndex<string, FileEntry>("OriginPath", unique=true)

module DefaultNames =
    [<Literal>]
    let FileName = ".fileshuffle"

type FileStructureStorage(filePath: string) =
    let db = StorageFactory.Instance.CreateStorage()

    do  db.Open(filePath, 4L*1024L*1024L)
    let mutable root = match db.Root with
                       | :? StorageRoot as r-> r
                       | _ -> let r = StorageRoot(db)
                              db.Root <- r
                              r
    interface IDisposable with
        member this.Dispose() = db.Close()

    member this.GetOriginalSourceFiles() = root.Files
                                           |> Seq.filter (fun entry -> entry.Location = LocationType.Origin)
                                           |> Seq.toList
    member this.GetShuffledFiles() = root.Files
                                     |> Seq.filter (fun entry -> entry.Location = LocationType.ShuffleFolder)
                                     |> Seq.toList
    member this.IterateAll() = root.Files :> Collections.Generic.IEnumerable<FileEntry>
    member this.Store(filePaths: Collections.Generic.IEnumerable<string>) =
        root.Deallocate()
        root <- StorageRoot(db)
        db.Root <- root
        let focusEntries = query {
                                for path in filePaths do
                                let entry = FileEntry.FromFilePath(path)
                                where (not <| String.IsNullOrEmpty(Path.GetDirectoryName(entry.OriginPath))
                                       && not <| ShuffleStorage.IsShuffleDirectory(entry.OriginPath))
                                select entry
                           }
        root.Files.BulkLoad(focusEntries)
        root.Files.Count
