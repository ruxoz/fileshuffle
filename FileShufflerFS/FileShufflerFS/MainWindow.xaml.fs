﻿namespace FileShuffler

open System.Windows
open RZ.Wpf

type MainWindowView() as this =
    inherit Window()

    do ignore <| XamlLoader.LoadWpf("MainWindow.xaml", Some (this :> obj))

    interface IXamlConnector with
        member this.Ready() =
            let scope = this.FindName("scope") :?> MainWindowModel
            scope.RefreshDrives()

