﻿namespace FileShuffler

open System

type ExceptionMessage = { message: string; original: string }
type DebugMessage = { message: string; level: string }

type ILog = 
    [<CLIEvent>]
    abstract member Log: IDelegateEvent<Action<string,obj>>
    abstract Info: obj -> unit

type LogBroadcaster() =
    let logEvent = DelegateEvent<Action<string,obj>>()

    interface ILog with
        [<CLIEvent>]
        member this.Log = logEvent.Publish
        member this.Info(data) = logEvent.Trigger([| "Info"; data |])