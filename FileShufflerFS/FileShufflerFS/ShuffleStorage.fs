﻿namespace FileShuffler

open System
open System.IO

type private ShuffleStorage(rootDirectory, log: ILog) as this =
    static let ShuffleFolderPrefix = "_shuffle"
    static let DefaultShuffleFolder = ShuffleFolderPrefix + "{0}"
    static member val public FilesInDirectoryLimit = 512

    [<DefaultValue>] val mutable shuffleDirectory : string
    [<DefaultValue>] val mutable directoryPrefixNumber : int
    [<DefaultValue>] val mutable duplicationPrefixNumber : int
    [<DefaultValue>] val mutable fileCounter : int

    let ensureShuffleDirectory(driveRoot) =
        this.shuffleDirectory <- Path.Combine(driveRoot, String.Format(DefaultShuffleFolder, this.directoryPrefixNumber.ToString()))
        if not <| Directory.Exists(this.shuffleDirectory) then
            ignore <| Directory.CreateDirectory(this.shuffleDirectory)

    static let getPathWithoutDrive(fullPath: string) =
        assert (fullPath.[1] = ':')
        fullPath.Substring(3)
        
    do ensureShuffleDirectory(rootDirectory)

    static member public IsShuffleDirectory(pathNoRoot: string) = pathNoRoot.StartsWith(ShuffleFolderPrefix)

    member this.RestoreFile(fileEntry: FileEntry) =
        let sourcePath = Path.Combine(rootDirectory, fileEntry.OriginPath)
        let targetPath = Path.Combine(rootDirectory, fileEntry.ShuffledPath)

        match File.Exists(targetPath) with
        | false ->
            try
                File.Move(targetPath, sourcePath)
            with
            | :? IOException as ex -> log.Info({ message=sprintf "Cannot move %s to %s" sourcePath targetPath; original=ex.Message})
        | true -> log.Info({ message=sprintf "File %s not found. Skip restoration for this file." fileEntry.ShuffledPath; level="warning"})

    member this.TakeFrom(path) =
        let originPath = Path.Combine(rootDirectory, path)
        let fileName = Path.GetFileName(originPath)
        assert (not <| String.IsNullOrEmpty(fileName))
        let mutable targetPath = Path.Combine(this.shuffleDirectory, fileName)
        while File.Exists(targetPath) do
            this.duplicationPrefixNumber <- this.duplicationPrefixNumber + 1
            targetPath <- Path.Combine(this.shuffleDirectory, (sprintf "[Dup%d] %s" this.duplicationPrefixNumber fileName))

        File.Move(originPath, targetPath)
        this.fileCounter <- this.fileCounter + 1
        if this.fileCounter = ShuffleStorage.FilesInDirectoryLimit then
            this.directoryPrefixNumber <- this.directoryPrefixNumber + 1
            ensureShuffleDirectory(rootDirectory)

        getPathWithoutDrive(targetPath)