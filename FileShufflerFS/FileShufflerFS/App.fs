﻿module MainApp

open System
open System.Windows
open FileShuffler

[<EntryPoint>]
[<STAThread>]
let main _ =
    let win = MainWindowView()
    Application().Run(win)