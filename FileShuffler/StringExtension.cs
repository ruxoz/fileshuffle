﻿using System;
using System.Text;

namespace RZ.Extensions {
    public static class StringExtension {
        public static String RemoveChar(this String s, Char c1, Char c2) {
            var sb = new StringBuilder(s.Length);

            foreach (Char c in s)
                if (c != c1 && c != c2)
                    sb.Append(c);

            return sb.ToString();
        }
        static public string Args(this string s, object arg0){
            return String.Format(s, arg0);
        }
        static public string Args(this string s, object arg0, object arg1){
            return String.Format(s, arg0, arg1);
        }
        static public string Args(this string s, object arg0, object arg1, object arg2){
            return String.Format(s, arg0, arg1, arg2);
        }
    }
}
