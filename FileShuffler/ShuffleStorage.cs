﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using RZ.Extensions;

namespace FileShuffler{
    class ShuffleStorage{
        const string ShuffleFolderPrefix = "_shuffle";
        const string DefaultShuffleFolder = ShuffleFolderPrefix + "{0}";
        public const int FilesInDirectoryLimit = 512;

        #region ctors
        public ShuffleStorage(string driveRoot, ILog log){
            rootDirectory = driveRoot;
            this.log = log;
            ensureShuffleDirectory(driveRoot);
        }
        #endregion

        public static bool IsShuffleDirectory(string pathNoRoot){
            return pathNoRoot.StartsWith(ShuffleFolderPrefix);
        }
        public void RestoreFile(FileEntry fileEntry){
            var sourcePath = Path.Combine(rootDirectory, fileEntry.OriginPath);
            var targetPath = Path.Combine(rootDirectory, fileEntry.ShuffledPath);

            if (File.Exists(targetPath))
                try{
                    File.Move(targetPath, sourcePath);
                } catch(IOException ex){
                    log.Info(new{
                        message = "Cannot move {0} to {1}".Args(sourcePath, targetPath),
                        original = ex.Message
                    });
                }
            else
                Trace.WriteLine("File {0} not found. Skip restoration for this file.".Args(fileEntry.ShuffledPath));
        }
        public string TakeFrom(string path) {
            var originPath = Path.Combine(rootDirectory, path);
            var fileName = Path.GetFileName(originPath);
            Debug.Assert(fileName != null);
            var targetPath = Path.Combine(shuffleDirectory, fileName);
            while (File.Exists(targetPath))
                targetPath = Path.Combine(shuffleDirectory, "[Dup{0}] {1}".Args((++duplicationPrefixNumber).ToString(CultureInfo.InvariantCulture), fileName));
            File.Move(originPath, targetPath);
            if (++fileCounter == FilesInDirectoryLimit){
                ++directoryPrefixNumber;
                ensureShuffleDirectory(rootDirectory);
            }
            return getPathWithoutDrive(targetPath);
        }
        void ensureShuffleDirectory(string driveRoot){
            shuffleDirectory = Path.Combine(driveRoot, DefaultShuffleFolder.Args(directoryPrefixNumber.ToString(CultureInfo.InvariantCulture)));
            if (!Directory.Exists(shuffleDirectory))
                Directory.CreateDirectory(shuffleDirectory);
        }
        static string getPathWithoutDrive(string fullPath){
            // WARNING: Windows specific implementation currently.
            Debug.Assert(fullPath[1] == ':', "Not Windows?? must implement for it");

            return fullPath.Substring(3);
        }
        readonly string rootDirectory;
        readonly ILog log;
        string shuffleDirectory;
        int fileCounter;
        int directoryPrefixNumber;
        int duplicationPrefixNumber;
    }
}