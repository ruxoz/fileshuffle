﻿using System;

namespace FileShuffler{
    public interface ILog{
        event Action<string, object> Log;
        void Info(object data);
    }
    class LogBroadcaster : ILog{
        public event Action<string, object> Log;
        public void Info(object data){
            var handler = Log;
            if (handler != null)
                handler("info", data);
        }
    }
}