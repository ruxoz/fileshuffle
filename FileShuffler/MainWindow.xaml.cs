﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using RZ.Extensions;

namespace FileShuffler {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        readonly ILog log;
        public MainWindow(ILog log) {
            this.log = log;
            structureStorage = new Lazy<FileStructureStorage>(
                delegate{
                            var filePath = Path.Combine(scope.SelectedDrive.RootDirectory.FullName, FileStructureStorage.DefaultFileName);
                            return new FileStructureStorage(filePath);
                        });
            InitializeComponent();

            scope.Progress = new MainWindowModel.ProgressBar();
            scope.Logs = new List<string>();
            scope.Drives = DriveInfo.GetDrives();

            log.Log += delegate(string s, object o){
                scope.Logs.Add(s + ": " + o);
                scope.OnPropertyChanged("Logs");
            };
        }
        FileStructureStorage Storage{
            get { return structureStorage.Value; }
        }
        private void onCleanupBeforeClosing(object sender, System.ComponentModel.CancelEventArgs e){
            e.Cancel = false;
            if (structureStorage.IsValueCreated)
                Storage.Dispose();
        }
        private void onRestoreStructure(object sender, RoutedEventArgs e) {
            status("Getting file list...");
            var entries = Storage.GetShuffledFiles();

            scope.Progress.Count = entries.Length;
            scope.Progress.Value = 0;
            status("Restoring...");
            restoreFiles(entries);
        }
        void restoreFiles(IEnumerable<FileEntry> fileIterator){
            var entries = fileIterator.ToArray();
            IsEnabled = false;
            var selectedDrive = scope.SelectedDrive.RootDirectory.FullName;
            Task.Factory.StartNew(() => trapException(() => restoreFileStructure(selectedDrive, entries)));
        }
        void restoreFileStructure(string selectedDrive, FileEntry[] entries){
            status("Getting file list...");
            var shuffleDir = new ShuffleStorage(selectedDrive, log);

            resetProgress(entries.Length);
            status("Restoring structure...");
            foreach (var fileEntry in entries) {
                shuffleDir.RestoreFile(fileEntry);
                fileEntry.Location = LocationType.Origin;
                fileEntry.Modify();
                increaseProgress();
            }
            MessageBox.Show("File Structure is restored.");

            Dispatcher.Invoke(new Action(() => IsEnabled = true));
        }
        void onSaveFileStructure(object sender, RoutedEventArgs e){
            status();
            var rootDir = scope.SelectedDrive.RootDirectory;

            var fileEntries = rootDir.GetFileSystemInfos("*", SearchOption.AllDirectories);

            const FileAttributes excludeFile = (FileAttributes.System | FileAttributes.Temporary | FileAttributes.Directory);
            var stored = Storage.Store(from entry in fileEntries
                          where (entry.Attributes & excludeFile) == 0
                          select entry.FullName);

            status("{0} entries have been saved.".Args(stored.ToString(CultureInfo.InvariantCulture)));
            MessageBox.Show("File Structure is stored!");
        }
        void onShuffle(object sender, RoutedEventArgs e){
            IsEnabled = false;
            var storage = Storage;
            var selectedDrive = scope.SelectedDrive.RootDirectory.FullName;
            Task.Factory.StartNew(() => trapException(() => shuffle(storage, selectedDrive)));
        }
        void shuffle(FileStructureStorage storage, string selectedDrive){
            status("Getting file list...");
            var entries = storage.GetOriginalSourceFiles();

            resetProgress(entries.Length);
            status("Shuffling...");
            shuffle(entries);

            resetProgress(entries.Length);
            status("Moving file...");
            moveFiles(selectedDrive, entries);

            MessageBox.Show("Files are randomized and rearranged!");

            Dispatcher.Invoke(new Action(() => IsEnabled = true));
        }
        private void onViewStorage(object sender, RoutedEventArgs e){
            StorageView.Show(Storage);
        }
        void resetProgress(int maxValue){
            var action = new Action(delegate{
                                        scope.Progress.Count = maxValue;
                                        scope.Progress.Value = 0;
                                    });
            Dispatcher.Invoke(action);
        }
        void increaseProgress(){
            Dispatcher.Invoke(new Action(() => scope.Progress.Value += 1));
        }
        void moveFiles(string rootPath, IEnumerable<FileEntry> entries){
            var shuffleDir = new ShuffleStorage(rootPath, log);
            foreach(var fileEntry in entries){
                var shuffledPath = shuffleDir.TakeFrom(fileEntry.OriginPath);
                fileEntry.ShuffledPath = shuffledPath;
                fileEntry.Location = LocationType.ShuffleFolder;
                fileEntry.Modify();
                increaseProgress();
            }
        }
        void shuffle(FileEntry[] entries){
            for(var i=0; i < entries.Length; ++i){
                var target = i;
                while (target == i)
                    target = random.Next(entries.Length);
                var temp = entries[target];
                entries[target] = entries[i];
                entries[i] = temp;
                increaseProgress();
            }
        }
        void status(string text = ""){
            Dispatcher.Invoke(() => scope.StatusText = text);
        }
        static void trapException(Action action){
            try{
                action();
            } catch (Exception ex) {
                MessageBox.Show(ex.ToString(), "Shuffle is interrupted", MessageBoxButton.OK,
                                MessageBoxImage.Error);
            }
        }
        readonly Random random = new Random();
        readonly Lazy<FileStructureStorage> structureStorage;
    }
}
