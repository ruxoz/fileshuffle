﻿using System.Linq;
using System.Windows;

namespace FileShuffler {
    /// <summary>
    /// Interaction logic for StorageView.xaml
    /// </summary>
    public partial class StorageView{
        public StorageView() {
            InitializeComponent();
        }
        public static void Show(FileStructureStorage storage){
            var view = new StorageView();
            view.populateView(storage);
            view.ShowDialog();
        }
        void populateView(FileStructureStorage storage){
            var data = storage.IterateAll().Select(entry => new FileEntryView(entry)).ToArray();
            scrStorage.ItemsSource = data;
        }
        public class FileEntryView{
            public FileEntryView(FileEntry entry){
                this.entry = entry;
            }
            public string Origin{
                get { return entry.OriginPath; }
            }
            public string ShuffledPath{
                get { return entry.ShuffledPath; }
            }
            public LocationType Location{
                get { return entry.Location; }
            }
            readonly FileEntry entry;
        } 
    }
}
