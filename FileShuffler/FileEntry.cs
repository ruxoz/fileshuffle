﻿using System.Diagnostics;
using System.IO;
using Perst;

namespace FileShuffler{
    public enum LocationType {
        Origin,
        ShuffleFolder,
    }
    public class FileEntry : Persistent {
        /// <summary>
        /// Pure path without drive.
        /// </summary>
        public string OriginPath;
        /// <summary>
        /// Pure path without drive in the shuffle directory.
        /// </summary>
        public string ShuffledPath;
        public LocationType Location;

        public static FileEntry FromFilePath(string path) {
            return new FileEntry { OriginPath = getPathOnly(path), Location = LocationType.Origin };
        }
        static string getPathOnly(string path) {
            var pathRoot = Path.GetPathRoot(path);
            Debug.Assert(pathRoot != null);
            return path.Substring(pathRoot.Length);
        }
    }
}