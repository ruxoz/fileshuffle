using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Perst;

namespace FileShuffler{
    public class FileStructureStorage : IDisposable{
        public const string DefaultFileName = ".fileshuffle";

        #region ctors
        public FileStructureStorage(string filePath){
            db.Open(filePath, 4*1024*1024);
            root = (StorageRoot) db.Root;
            if (root == null)
                db.Root = root = new StorageRoot(db);
        }
        public void Dispose(){
            db.Close();
        }
        #endregion

        /// <summary>
        /// Store the entry file system in the database.
        /// </summary>
        /// <param name="filePaths">Full path, including drive, which is assumed to be in the same drive.</param>
        public int Store(IEnumerable<string> filePaths){
            root.Deallocate();
            db.Root = root = new StorageRoot(db);

            var focusEntries = from path in filePaths
                              let entry = FileEntry.FromFilePath(path)
                              where Path.GetDirectoryName(entry.OriginPath) != String.Empty
                                    && !ShuffleStorage.IsShuffleDirectory(entry.OriginPath)
                              select entry;
            root.Files.BulkLoad(focusEntries);
            return root.Files.Count;
        }
        public FileEntry[] GetOriginalSourceFiles(){
            return root.Files.Where(entry => entry.Location == LocationType.Origin).ToArray();
        } 
        public FileEntry[] GetShuffledFiles(){
            return root.Files.Where(entry => entry.Location == LocationType.ShuffleFolder).ToArray();
        }
        public IEnumerable<FileEntry> IterateAll(){
            return root.Files;
        }

        class StorageRoot : Persistent{
            public StorageRoot(Storage db){
                Files = db.CreateFieldIndex<string, FileEntry>("OriginPath", unique: true);
            }
            public readonly FieldIndex<string,FileEntry> Files;
        }

        readonly Storage db = StorageFactory.Instance.CreateStorage();
        StorageRoot root;
    }
}