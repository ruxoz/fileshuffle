﻿using System.Windows;

namespace FileShuffler {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        protected override void OnStartup(StartupEventArgs e){
            base.OnStartup(e);

            var window = new MainWindow(new LogBroadcaster());
            window.Show();
        }
    }
}
