﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Documents;
using FileShuffler.Annotations;

namespace FileShuffler
{
    internal class MainWindowModel : INotifyPropertyChanged
    {
        public class ProgressBar : INotifyPropertyChanged
        {
            int count;
            int value;
            public int Count
            {
                get { return count; }
                set
                {
                    if(value == count) return;
                    count = value;
                    OnPropertyChanged();
                }
            }
            public int Value
            {
                get { return value; }
                set
                {
                    if(value == this.value) return;
                    this.value = value;
                    OnPropertyChanged();
                }
            }
            public event PropertyChangedEventHandler PropertyChanged;
            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
            {
                var handler = PropertyChanged;
                if(handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public List<string> Logs{
            get { return logs; }
            set{
                if (Equals(logs, value))
                    return;
                logs = value;
                OnPropertyChanged();
            }
        }
        public DriveInfo[] Drives
        {
            get { return drives; }
            set
            {
                if(Equals(value, drives)) return;
                drives = value;
                OnPropertyChanged();
            }
        }
        public ProgressBar Progress
        {
            get { return progress; }
            set
            {
                if(Equals(value, progress)) return;
                progress = value;
                OnPropertyChanged();
            }
        }
        public int SelectedDriveIndex
        {
            get { return selectedDriveIndex; }
            set
            {
                if(value == selectedDriveIndex) return;
                selectedDriveIndex = value;
                OnPropertyChanged();
            }
        }
        public DriveInfo SelectedDrive
        {
            get { return drives[selectedDriveIndex]; }
        }
        public string StatusText
        {
            get { return statusText; }
            set
            {
                if(value == statusText) return;
                statusText = value;
                OnPropertyChanged();
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if(handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        DriveInfo[] drives;
        int selectedDriveIndex;
        string statusText;
        ProgressBar progress;
        List<string> logs;
    }
}